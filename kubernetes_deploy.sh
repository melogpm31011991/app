#!/bin/bash

# Instalacao do kubectl
K8S_VERSION=$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)
curl -L https://storage.googleapis.com/kubernetes-release/release/${K8S_VERSION}/bin/linux/amd64/kubectl -o /usr/bin/kubectl
chmod +x /usr/bin/kubectl
aws eks --region=us-east-2 update-kubeconfig --name training-eks-PvoXkb6x

# Preparacao do arquivo de configuracao do login no K8s
#CONF_FILE="
#apiVersion: v1
#clusters:
#- cluster:
#    certificate-authority-data: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUN5RENDQWJDZ0F3SUJBZ0lCQURBTkJna3Foa2lHOXcwQkFRc0ZBREFWTVJNd0VRWURWUVFERXdwcmRXSmwKY201bGRHVnpNQjRYRFRJd01USXhOakUxTWpNd01Gb1hEVE13TVRJeE5ERTFNak13TUZvd0ZURVRNQkVHQTFVRQpBeE1LYTNWaVpYSnVaWFJsY3pDQ0FTSXdEUVlKS29aSWh2Y05BUUVCQlFBRGdnRVBBRENDQVFvQ2dnRUJBTXd4CmxLZjFoVmxwUTZEN1BvSGVDbUdnQkswZWhmdG4xQzF5T3Y4dXNKeUxpbFNFRVlzdGxBbnlzN05xN2VYSXExdlcKM1pBcVlyWnlzQ2VlakU0SkpCeHIyelh1anlSMUU1RExRbXhScE5idlByWnRlQ3RpdW9SaHRZSi9UOU5YeFUxdAp2OHlSSVppa1ZSZC82b0ZZM1loeE85Z0Q1TVRveEZaaTN1OUlwbzQ3MjQyNFBFRTFxamczVTBMZ0FoZzQrc0twCklGUWloOVVqRys0WmV2cHFWK0FoLzVMUERMRENkSE9peWFOalZmM3F2bUFPZDA4MGFnamdUZzVocHNXYUNnSlIKVGtFeWRSWUVLV1l0TW5RbElzWm1keFcvQVNFVHV3bm5nTkQvWnFwaU5Ra2pJb1liTzQva0p2MnpTWEpRMndCaAo4WXcwQ0ExK2FkOHdSa0VyOVNrQ0F3RUFBYU1qTUNFd0RnWURWUjBQQVFIL0JBUURBZ0trTUE4R0ExVWRFd0VCCi93UUZNQU1CQWY4d0RRWUpLb1pJaHZjTkFRRUxCUUFEZ2dFQkFMOWRNT1pYMjIxUU1xWEdYTjFLSjlwQlJSaXEKK1dzWGpCR0pCY281SmhqL1VPSGtQOU4zaTRWUGt6bFlyLy8xUC9vUjh4UUNHWkxEbmdKUWpOSHkrYTk3L293TwoyNHhkd3hNRDNmRUZqNGIydjV1SUJJMVF5NDlqSnl2MDVUN1Npa1BVTVREajlGb29CYys3N283ZkljWXc4UCtGCnA3MldaN29FWE94U3M3Mm80d28yUlhnNTVYNm84WnpCaWtPdy9SKy9Ka2RyNm45THFKNjB0Ry9FdFpPVXl0eGwKVmRRUytBcVZRb3hOSnE4cXRXSVZ1M05lbTNGTDZKbHUyN1BnVmpFeSt5cWhabzJwalRWN2RQbTdmY3RuUjkvWgoxM0RaZmhLUm85THlweldZZkh3ZVo1Vkx0US96ZTBLbW5nMHpERVZJWjlRblQxbGFRNXRMTFgya3pKaz0KLS0tLS1FTkQgQ0VSVElGSUNBVEUtLS0tLQo=
#    server: https://874F09B5E71079922C9787B04A5902A3.yl4.us-east-2.eks.amazonaws.com
#  name: arn:aws:eks:us-east-2:673590449241:cluster/training-eks-PvoXkb6x
#contexts:
#- context:
#    cluster: arn:aws:eks:us-east-2:673590449241:cluster/training-eks-PvoXkb6x
#    user: arn:aws:eks:us-east-2:673590449241:cluster/training-eks-PvoXkb6x
#  name: arn:aws:eks:us-east-2:673590449241:cluster/training-eks-PvoXkb6x
#current-context: arn:aws:eks:us-east-2:673590449241:cluster/training-eks-PvoXkb6x
#kind: Config
#preferences: {}
#users:
#- name: arn:aws:eks:us-east-2:673590449241:cluster/training-eks-PvoXkb6x
#  user:
#    exec:
#      apiVersion: client.authentication.k8s.io/v1alpha1
#      args:
#      - --region
#      - us-east-2
#      - eks
#      - get-token
#      - --cluster-name
#      - training-eks-PvoXkb6x
#      command: aws
#
#"
#mkdir ~/.kube/
#echo "${CONF_FILE}" > ~/.kube/config
cat ~/.kube/config
#echo ${K8S_KEY}

# Deploy
echo "Realizando deploy da imagem: ${IMAGE_NAME}"
kubectl get pods -A
kubectl get nodes 
kubectl run --image=${IMAGE_NAME}:${APP_NAME} ${APP_NAME1} --port=8888
kubectl get pods -A

#kubectl set image -n ${APP_NAMESPACE} deployments/${APP_NAME} ${APP_NAME}=${IMAGE_NAME}
