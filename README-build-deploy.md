# Simple Flask Application

## Up and Running

To get the application up and running follow these steps:

#build
1. Create and activate a virtual environment:

```bash
$ python -m venv venv
$ source venv/bin/activate
```
2. Install the dependencies:

```bash
$ pip install -r requirements.txt
```

#deploy
3. Start the development server:

```bash
$ APP_SECRET_TOKEN=SomeSecretToken python3.8 app.py
```
