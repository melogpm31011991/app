# Part 2
# PORTUGUESE
# Rota 53
O Route 53 é o serviço DNS globalmente disponível da Amazon. Com o DNS, "cache" assume um significado um pouco diferente. Como no cache do lado do cliente, você não controla o cache tanto quanto passa diretivas, instruindo o cliente sobre como fazer o cache. Nesse caso, a configuração de valores TTL sensíveis para seus registros DNS permitirá que os clientes confiem em resolvedores DNS locais. TTLs baixos significam que eles precisam realizar uma pesquisa DNS completa com mais frequência, potencialmente adicionando preciosos milissegundos ao tempo de resposta do seu aplicativo.

# CloudFront

CloudFront é um CDN fornecido pela Amazon. O armazenamento em cache de conteúdo estático, como arquivos de imagem e CSS na camada do CloudFront, reduzirá muito a largura de banda consumida e o tempo de resposta. Aqueles que visitam seu site ou usam seu aplicativo não ficarão esperando por chamadas de ida e volta à API ou invocações Lambda para ter uma experiência de usuário funcional.

# API Gateway

Um Amazon API Gateway é um conjunto de recursos e métodos integrados a endpoints HTTP de back-end, funções Lambda ou outros serviços AWS. É um serviço totalmente gerenciado que torna fácil para os desenvolvedores publicar, manter, monitorar e proteger APIs em qualquer escala, fornecendo aos desenvolvedores um serviço simples, flexível e com pagamento conforme o uso, que lida com todos os aspectos da criação e operação de APIs robustas para back-ends de aplicativos. 
Junto com o Lambda, o API Gateway forma a parte voltada para o aplicativo da infraestrutura sem servidor da AWS. 

# Endpoints
Um  tipo de terminal de API se refere ao nome do host da API.
O tipo de endpoint da API pode ser  otimizado por borda ,  regional ou  privado , dependendo de onde a maior parte do tráfego da API se origina.

# Tipos de dados usados ​​com o API Gateway:

Qualquer carga enviada por HTTP (sempre criptografada por HTTPS).
Os formatos de dados incluem JSON, XML, parâmetros de string de consulta e cabeçalhos de solicitação.
Você pode declarar qualquer tipo de conteúdo para suas respostas de APIs e, em seguida, usar os modelos de transformação para alterar a resposta de back-end para o formato desejado.

# Edge-Optimized Endpoint:

Um endpoint de API com otimização de borda é melhor para clientes distribuídos geograficamente. As solicitações de API são roteadas para o CloudFront Point of Presence (POP) mais próximo. Este é o tipo de terminal padrão para APIs REST de gateway de API.
APIs otimizadas para borda capitalizam os nomes de cabeçalhos HTTP (por exemplo, Cookie).
CloudFront classifica os cookies HTTP em ordem natural por nome do cookie antes de encaminhar a solicitação para sua origem. Para obter mais informações sobre a maneira como o CloudFront processa cookies, consulte Cache de conteúdo com base em cookies.
Qualquer nome de domínio personalizado que você usa para uma API otimizada de borda se aplica a todas as regiões.

# VPC
Controle seu ambiente de redes virtuais, incluindo a seleção do seu próprio intervalo de endereços IP, a criação de sub-redes e a configuração de tabelas de rotas e gateways de rede. Personalize a configuração da rede, por exemplo, criando uma sub-rede voltada para o público com foco nos servidores Web que tenham acesso à Internet e colocar seus sistemas back-end, como bancos de dados ou servidores de aplicativos em uma sub-rede de uso privado sem acesso à Internet.
# Lambda

Instancie clientes AWS e clientes de banco de dados fora do escopo do manipulador para aproveitar a reutilização da conexão.
• Agende eventos com CloudWatch para calor
• ENIs para suporte VPC são anexados durante a inicialização a frio

# RDS

O Amazon Relational Database Service (Amazon RDS) facilita a configuração, a operação e a escalabilidade de bancos de dados relacionais na nuvem. O serviço oferece capacidade econômica e redimensionável e automatiza tarefas demoradas de administração, como provisionamento de hardware, configuração de bancos de dados, aplicação de patches e backups. Dessa forma, você pode se concentrar na performance rápida, alta disponibilidade, segurança e conformidade que os aplicativos precisam.
O Amazon RDS está disponível em vários tipos de instância de banco de dados – com otimização para memória, performance ou E/S – e oferece seis mecanismos de bancos de dados comuns, incluindo Amazon Aurora, PostgreSQL, MySQL, MariaDB, Oracle Database e SQL Server. Você pode usar o AWS Database Migration Service para migrar ou replicar facilmente bancos de dados existentes para o Amazon RDS.

# O cliente 
O cliente é tecnicamente apenas mais uma camada na pilha, embora uma sobre a qual você realmente não tenha muito controle. No entanto, isso não significa que você não pode usar o cliente para melhorar o desempenho do aplicativo e potencialmente economizar nos custos de largura de banda. 

Usando certas diretivas de cabeçalho, você pode instruir o navegador do cliente a armazenar a resposta em cache. Se o cliente puder buscar conteúdo ou dados de resposta localmente, você verá uma melhora imediata e óbvia no desempenho e, como bônus, o aplicativo não consumirá largura de banda de rede adicional até que uma solicitação de novos dados seja feita.

# Resumo

É um site foi configurado o dns no route 53 que bate no CloudFront, que esá funcionando como um CDN e que está conectado ao S3, que por sua vez tem uma camada de apis que esta configurado no API Gateway, que tem um triger para um lambda que está dentro de uma VPC que faz o imput do banco de dados RDS. 


# ENGLISH
# Route 53
Route 53 is Amazon's globally available DNS service. With DNS, "cache" takes on a slightly different meaning. As with client-side caching, you don't control the cache as much as passing policies, instructing the client on how to cache it. In this case, setting sensitive TTL values ​​for your DNS records will allow customers to trust local DNS resolvers. Low TTLs means they need to do a full DNS lookup more often, potentially adding precious milliseconds to your application's response time.

# CloudFront

CloudFront is a CDN provided by Amazon. Caching static content, such as image and CSS files in the CloudFront layer, will greatly reduce the bandwidth consumed and the response time. Those who visit your website or use your application will not be waiting for roundtrip calls to the API or Lambda invocations to have a functional user experience.

# API Gateway

An Amazon API Gateway is a set of features and methods integrated with backend HTTP endpoints, Lambda functions, or other AWS services. It is a fully managed service that makes it easy for developers to publish, maintain, monitor, and protect APIs at any scale, providing developers with a simple, flexible, and pay-as-you-go service that handles all aspects of API creation and operation robust for application backends.
Along with Lambda, API Gateway forms the application-oriented part of the AWS serverless infrastructure.

# Endpoints
An API endpoint type refers to the hostname of the API.
The type of API endpoint can be optimized per edge, regional or private, depending on where most of the API traffic originates.

# Data types used with API Gateway:

Any payload sent over HTTP (always encrypted over HTTPS).
The data formats include JSON, XML, query string parameters, and request headers.
You can declare any type of content for your API responses and then use the transformation models to change the backend response to the desired format.

# Edge-Optimized Endpoint:

An edge-optimized API endpoint is best for geographically distributed customers. API requests are routed to the nearest CloudFront Point of Presence (POP). This is the default endpoint type for API gateway REST APIs.
Edge-optimized APIs capitalize the names of HTTP headers (for example, Cookie).
CloudFront sorts HTTP cookies in natural order by cookie name before forwarding the request to its origin. For more information about the way CloudFront processes cookies, see Cache content based on cookies.
Any custom domain name you use for an edge-optimized API applies to all regions.

# VPC
Amazon VPC helps you control your virtual networking environment by letting you choose your own IP Address range, create your own subnets, and configure route tables to any available gateways. You can customize the network configuration by creating a public-facing subnet for your web servers that has access to the internet. Place your backend systems, such as databases or application servers, in a private-facing subnet. With Amazon VPC, you can ensure that your virtual private cloud is configured to fit your specific business needs.
# Lambda

Instantiate AWS clients and database clients outside the scope of the handler to take advantage of connection re-use.
• Schedule with CloudWatch Events for warmth
• ENIs for VPC support are attached during cold start
# RDS

Amazon Relational Database Service (Amazon RDS) makes it easy to set up, operate, and scale a relational database in the cloud. It provides cost-efficient and resizable capacity while automating time-consuming administration tasks such as hardware provisioning, database setup, patching and backups. It frees you to focus on your applications so you can give them the fast performance, high availability, security and compatibility they need.

Amazon RDS is available on several database instance types - optimized for memory, performance or I/O - and provides you with six familiar database engines to choose from, including Amazon Aurora, PostgreSQL, MySQL, MariaDB, Oracle Database, and SQL Server. You can use the AWS Database Migration Service to easily migrate or replicate your existing databases to Amazon RDS.

# The client
The client is technically just another layer on the stack, although one over which you don't have much control. However, this does not mean that you cannot use the client to improve the performance of the application and potentially save on bandwidth costs.

Using certain header policies, you can instruct the customer's browser to cache the response. If the customer can search for content or response data locally, you will see an immediate and obvious improvement in performance and, as a bonus, the application will not consume additional network bandwidth until a request for new data is made.

# Resume

It is a website that configured the DNS on route 53 that hits CloudFront, which is functioning as a CDN and that is connected to S3, which in turn has an APIs layer that is configured in API Gateway, which has a trigger for one lambda that is inside a VPC that inputs the RDS database.

# Referência - Reference


https://digitalcloud.training/certification-training/aws-solutions-architect-associate/networking-and-content-delivery/amazon-api-gateway/

https://blog.thundra.io/caching-with-aws-serverless-applications

https://docs.aws.amazon.com/pt_br/Route53/latest/DeveloperGuide/routing-to-api-gateway.html

https://www2.slideshare.net/AmazonWebServices/serverless-architectural-patterns-and-best-practices?from_action=save

https://aws.amazon.com/vpc/?nc2=h_ql_prod_fs_vpc&vpc-blogs.sort-by=item.additionalFields.createdDate&vpc-blogs.sort-order=desc

https://aws.amazon.com/pt/rds/?nc2=h_ql_prod_db_rds