FROM python:3.8

RUN python -m venv venv
RUN . venv/bin/activate
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .
ENV APP_SECRET_TOKEN $APP_SECRET_TOKEN
#RUN APP_SECRET_TOKEN=$APP_SECRET_TOKEN python3.8 app.py
EXPOSE 8888/tcp
CMD ["python3.8", "app.py" ] 